#!/bin/bash

# install
yum -y install epel-release && yum -y install git ansible vim lftp mc screen

# prepare local ansible...
useradd ansible && echo EPAM | passwd --stdin ansible
echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
sudo -u ansible bash -c "ssh-keygen -f ~ansible/.ssh/id_rsa -N ''"

# git init in ansible homedir
cd /home/ansible/ && sudo -u ansible git clone https://gitlab.com/devops_epam/ansible_lab.git

# create ansible on remote nodes
cd /home/ansible/ansible_lab/ && sudo -u ansible ansible-playbook -i inventory create_ansible_sudo_user.yaml

# done
echo 'Done! Now, you can switch to ansible user and test my homework'

