# Ansible EPAM lab

# Step 0:
`vagrant up`

# Step 1:
copy and run as root lab_init.sh

# Step 3:
Just `su - ansible` and use some checks below...

_For check ansible run it_
`cd ansible_lab/ && ansible all -m ping`

# homework 1 checks:

_For check task 1_1 run it_
```
ansible-playbook task1/install_httpd.yaml
curl node1.example.com
```
_For check task 1_2 run it_
```
ansible-playbook task1/remove_httpd.yaml
curl node2.example.com
```
_For check task 1_3 run it_
```
ansible-playbook task1/grub_change.yaml
echo grep GRUB_CMDLINE_LINUX /etc/default/grub | ssh node1.example.com
```

# homework 2 checks:

_For check task 2 run it_
```
ansible-playbook task2/create_users.yaml --vault-password-file task2/vault_secret
ssh Alice@node1.example.com
ssh Bob@node1.example.com
ssh Carol@node1.example.com
```
_For remove it run:_
```
ansible-playbook task2/delete_users.yaml
```

# homework 3 checks:
```
ansible-playbook task3/main.yaml
curl node1.example.com
lftp node2.example.com
```
than put something into upload...

# homework 4 checks:
_just run:_
```
task4/install_awx.sh
```
