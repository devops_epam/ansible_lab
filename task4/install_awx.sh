#!/bin/bash

export AWX_VER='17.1.0'
# install
sudo yum -y install epel-release
sudo yum install -y ansible bash-completion git device-mapper-persistent-data lvm2 python3 vim yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable --now docker
sudo usermod -aG docker $(whoami)
newgrp docker
sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
sudo ln -sf /usr/bin/python3 /usr/bin/python
sudo pip3 install docker-compose
# done
# 
echo edit some files and prepare to run
cd ~/ansible_lab/ && git clone -b $AWX_VER https://github.com/ansible/awx
cd ~/ansible_lab/ && sed -i 's/# admin_password=password/admin_password=password/' awx/installer/inventory
cd ~/ansible_lab/ && sed -i 's/#project_data_dir=\/var\/lib\/awx\/projects/project_data_dir=\/var\/lib\/awx\/projects/g' awx/installer/inventory
# 
echo
cd ~/ansible_lab/awx/installer/
pwd
echo now you can try run awx:
echo 'ansible-playbook -i inventory install.yml'
