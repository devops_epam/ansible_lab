#!/bin/bash

# for remote push
git remote set-url origin https://gitlab.com/devops_epam/ansible_lab.git
git remote set-url --push origin ssh://git@gitlab.com/devops_epam/ansible_lab.git
git config pull.rebase false
git config --global user.email "kalashmatik@gmail.com"
git config --global user.name "Kalashmatik"

